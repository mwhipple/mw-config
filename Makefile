##
# Define project structure and global variables.
##

# Specify the root directory for user configuration.
CONFIG_INSTALL_ROOT   := ${HOME}/.config/
# Specify the host directory to store this project's configuration data.
MW_CONFIG_INSTALL_DIR := ${CONFIG_INSTALL_ROOT}mw-config/
OUT_DIRS              += ${MW_CONFIG_INSTALL_DIR}
# Define the host bash initialization script to modify.
export BASH_INIT      := ${HOME}/.bash_profile

# This project is very bash-y and some stuff will fail in other shells.
SHELL := $(shell which bash)

##
# Display general project information.
##

help: ; @cat README.rst
todo: ; @cat TODO

.PHONY: help todo

##
# Execute config_* scripts as needed.
##

CONFIGS        = config_bash config_git
CONFIG_MARKERS = $(addprefix ${MW_CONFIG_INSTALL_DIR}, ${CONFIGS})

${MW_CONFIG_INSTALL_DIR}config_%: config_% | ${MW_CONFIG_INSTALL_DIR}
	@./$<
	@touch $@

all: ${CONFIG_MARKERS}

##
# Copy bash includes to install directory.
##

INCLUDE_SRC_DIR     := bash.d/
INCLUDE_INSTALL_DIR := ${HOME}/.bash.d/
OUT_DIRS            += ${INCLUDE_INSTALL_DIR}
INCLUDE_SRCS         = $(shell git ls-files ${INCLUDE_SRC_DIR})
INSTALLED_INCLUDES   = $(subst ${INCLUDE_SRC_DIR},${INCLUDE_INSTALL_DIR},${INCLUDE_SRCS})

${INCLUDE_INSTALL_DIR}%: ${INCLUDE_SRC_DIR}% | ${INCLUDE_INSTALL_DIR}
	cp $< $@

all: ${INSTALLED_INCLUDES}

##
# Copy emacs files to emacs directory.
##

EMACS_SRC_DIR     := emacs.d/
EMACS_INSTALL_DIR := ${HOME}/.emacs.d/
OUT_DIRS          += ${EMACS_INSTALL_DIR}
EMACS_SRCS         = $(shell git ls-files ${EMACS_SRC_DIR})
INSTALLED_EMACS    = $(subst ${EMACS_SRC_DIR},${EMACS_INSTALL_DIR},${EMACS_SRCS})

${EMACS_INSTALL_DIR}%: ${EMACS_SRC_DIR}% | ${EMACS_INSTALL_DIR}
	cp $< $@

all: ${INSTALLED_EMACS}

##
# Copy gitignore to home directory.
##

export GITIGNORE_INSTALL_DIR := ${HOME}/
${GITIGNORE_INSTALL_DIR}.gitignore_global: .gitignore_global
	cp $< $@

all: ${GITIGNORE_INSTALL_DIR}.gitignore_global

##
# Assert that locally defined logic acts correctly.
##

check: ; source spec.sh && run_specs lib.sh

.PHONY: check

##
# Provide OS specific configuation
##

MAC_DIR := mac/
KERNEL  := $(shell uname)

ifeq (${KERNEL},Darwin)
#	include ${MAC_DIR}Makefile
endif

##
# Define some utility tasks.
##

${OUT_DIRS}: ; mkdir -p $@

clean: ; rm -rf ${OUT_DIRS}

.PHONY: clean all
