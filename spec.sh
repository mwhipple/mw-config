##
# @file spec.sh
# Support basic bash function unit testing.
#
# This feature is intended to be used by adding
# additional functions ending in '_spec' which verify the behavior of the
# functions under test. As bash functions tend to be fairly simple the
# normal pattern for writing a function funcName will consist of writing
# the documentation for the function, the funcName function itself,
# and then a funcName_spec function which serves to verify the behavior
# and also double as a living example of how to use the function.
# Additional mechanisms could be added to support more readable usage of
# these tests as examples...but given the nature of bash
# that is not a particularly likely pursuit.
#
# This file also includes a runner function: 'run_specs'. To run all of the
# specs in a file, this file itself can be sources and then that function
# can be run, being provided the files to test.
#
# The functionality here could be significantly gussied up, but that is
# only likely to happen in response to encountered needs.
#
# It should also be noted that provides very basic functionality which does
# not include features such as insulation of failures within the test...
# for example if a tested function kills the shell then it is likely to
# also kill the test runner. This can be handled in the test itself but
# if needed frequently then a more powerful alternative such as bats
# should be preferred.
##

##
# Capture the number of checks in the current test run that have passed.
##
declare -i assertions_passed=0

##
# Capture the number of checks in the current test run that have failed.
##
declare -i assertions_failed=0

##
# Capture the number of failed assertions within the currently evaluated spec.
##
declare -i spec_failures=0

##
# Capture the number of specs in the current test run that have passed.
##
declare -i specs_passed=0

##
# Capture the number of specs in the current test run that have failed.
##
declare -i specs_failed=0

##
# Check that the two provided strings are equal and track the results.
#
# If the strings are not equal, an informative message will be logged to stdout.
#
# The second argument is likely to be output produced in a subshell, such as:
#   check_equal 'foo' "$(set_foo && echo $VAL)"
#
# @param $1 Specify the string value that is expected to be matched.
# @param $2 Provie the actual string value against which the comparison will be done.
##
check_equal() {
    if [[ "$1" != "$2" ]]; then
	printf 'FAILURE %s:%s:%d: Expecting: %s but got: %s\n'\
	       "${BASH_SOURCE[1]}" "${FUNCNAME[1]}" "${BASH_LINENO[2]}" "$1" "$2"
	(( assertions_failed++ ))
	(( spec_failures++ ))
    else
	(( assertions_passed++ ))
    fi
}

##
# Check that the second parameter matches the pattern provided in the first.
#
# This uses Bash provided pattern matching, where glob style expressions are standard.
#
# @param $1 Specify the pattern which the second parameter should match.
# @param $2 Provide the input which should match the pattern specified in the first parameter.
##
check_match() {
    if [[ "$2" != $1 ]]; then
	printf 'FAILURE %s:%s:%d: Expecting to match: %s but got: %s\n'\
	       "${BASH_SOURCE[1]}" "${FUNCNAME[1]}" "${BASH_LINENO[2]}" "$1" "$2"
	(( assertions_failed++ ))
	(( spec_failures++ ))
    else
	(( assertions_passed++ ))
    fi
}

##
# Run all of the specs in the provided file; collect and display the results.
#
# This will produce a listing of counts for passing and failing specs and assertions.
# This will also exit with a status code of 1 if any failures are present.
#
# @param $1 Provide the name of the script to source to locate the '_spec' functions.
# @return Return 1 If there were any failures, otherwise 0.
##
run_specs() {
    (( assertions_passed = 0 ))
    (( assertions_failed = 0 ))
    (( specs_passed = 0 ))
    (( specs_failed = 0 ))

    source "$1"
    # Disable strictness when testing.
    # There's some quirky error around math on 0's with -u and this seems
    # generally likely to avoid logical errors getting hidden by strictness
    # which would be otherwise enforced anyway.
    set +euo pipefail

    for F in $(compgen -A function -X "!*_spec"); do
	local -i spec_failures=0
	eval "$F"
	if (( spec_failures > 0 )); then
	    (( specs_failed++ ))
	else
	    (( specs_passed++ ))
	fi
    done

    printf "\nSpecs\n-----\nPassed: %d\nFailed: %d\n\nAssertions\n----------\nPassed: %d\nFailed: %d\n\n"\
	   $specs_passed $specs_failed $assertions_passed $assertions_failed

    # Expression used for return value.
    (( specs_failed == 0 ))
}
