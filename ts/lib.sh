##
# Verify that the left version is not less than the right.
#
# @param $1 Provide the name of the command being tested.
# @param $2 Specify the version to test.
# @param $3 Specify the version which must be met or exceeded.
# @param $4 Define the error level to prefix to the message (defaults to WARN).
# @return Return 0 if version satisfied, otherwise 1.
##
verify_minimum_version() {
  [[ "$(echo -e "$2\n$3" | sort -V | tail -n 1)" == "$2" ]] || {
    error "${4:-WARN}: $1 should be at least version: $3 but was only: $2"
    return 1
  }
}
verify_minimum_version_spec() {
  check_equal '' "$(verify_minimum_version 'foo' '1.0' '1.0')"
  check_equal '' "$(verify_minimum_version 'foo' '2.0' '1.0')"
  check_equal '' "$(verify_minimum_version 'foo' '1.1' '1.0')"
  check_equal 'WARN: foo should be at least version: 1.0 but was only: 0.9' \
              "$(2>&1 verify_minimum_version 'foo' '0.9' '1.0')"
}


##
# Write the config to set an environment variable.
#
# This will do nothing if the desired value is
# set in the current environment.
# Any existing value will be output.
#
# @param $1 Define file to which the set command may be written.
# @param $2 Specify the environment variable whose value is being defined.
# @param $3 Specify the value to assign to the variable.
##
set_env() {
  if [[ "${!2}" != "$3" ]]; then
    echo "Updating $2 from ${!2} to $3"
    append_if_missing "$1" "export $2=\"$3\""
  fi
}
set_env_spec() {
  file="${TMPDIR}mw-config_append_if_missing"
  rm -f "${file}"

  # Unset environment variable
  set_env "${file}" 'MW_CONFIG_TEST' 'FOO'
  check_equal 'export MW_CONFIG_TEST="FOO"' "$(<"${file}")"

  # Won't overwrite existing line
  set_env "${file}" 'MW_CONFIG_TEST' 'FOO'
  check_equal 'export MW_CONFIG_TEST="FOO"' "$(<"${file}")"

  # Adds new line for changed value
  set_env "${file}" 'MW_CONFIG_TEST' 'BAR'
  check_equal $'export MW_CONFIG_TEST="FOO"\nexport MW_CONFIG_TEST="BAR"' "$(<"${file}")"

  # Won't write if reflected in current env
  export MW_CONFIG_TEST='BAZ'
  set_env "${file}" 'MW_CONFIG_TEST' 'BAZ'
  check_equal $'export MW_CONFIG_TEST="FOO"\nexport MW_CONFIG_TEST="BAR"' "$(<"${file}")"

  # Will write if current env doesn't match provided value
  set_env "${file}" 'MW_CONFIG_TEST' 'BIZ'
  check_equal $'export MW_CONFIG_TEST="FOO"\nexport MW_CONFIG_TEST="BAR"\nexport MW_CONFIG_TEST="BIZ"' "$(<"${file}")"
}

verify_minimum_version 'bash' "$BASH_VERSION" '4.0' 'ERROR'

##
# Log a message to stderr.
#
# @param $1 The message to send.
##
error() {
  echo "$1"
} >&2
