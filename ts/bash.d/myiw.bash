##
# Output status messages if set to anything non-empty.
##

##
# Set WLAN_IF to the detected wireless device name.
#
# This command should not be trusted to work on any system
# other than those currently used.
#
# This variable is used pervasively through the functions
# defined here.
##
declare WLAN_IF=${WLAN_IF:=$(basename $(dirname $(find /sys -name wireless)))}

##
# Pass iw commands to the defined WLAN_IF device.
#
# @param $@ commands and options to pass to iw
##
myiw::iw() {
  sudo iw dev "${WLAN_IF}" "$@"
}

##
# Produce a list of SSIDs with names matching the provided pattern.
#
# @param $1 The pattern which the returned SSIDs should match/contain
# @output Found SSIDs, one per line
##
myiw::match_ssids() {
  while read line; do
    if [[ "$line" =~ ^[[:blank:]]*SSID:\ (.*$1.*)$ ]]; then
      echo ${BASH_REMATCH[1]}
    fi
  done < <(myiw::iw scan)
}

##
# Pass iw commands to the defined $MY_IW device.
#
# Also supports subcommand 'scanconn' which accepts a pattern
# as an argument and will scan for and connect to any
# networks matching that pattern.
#
# For example: myiw scanconn MBTA
#
# @param $@ commands and options to pass to iw
##
myiw() {
  case "$1" in
    scanconn)
      [[ -z "${MYIW_VERBOSE}" ]] || echo "Scanning for $2" >&2
      ssid="$(myiw::match_ssids $2 | shuf -n 1)"
      [[ -z "${MYIW_VERBOSE}" ]] || echo "Found ${ssid}" >&2
      myiw::iw connect "${ssid}"
      [[ -z "${MYIW_VERBOSE}" ]] || echo "Connected to ${ssid}"
      ;;
    *)
      myiw::iw $@
      ;;
  esac
}
