##
# @file git-completion.bash
# bash/zsh completion support for core Git
#
# Copyright (C) 2006,2007 Shawn O. Pearce <spearce@spearce.org>
# Conceptually based on gitcompletion (http://gitweb.hawaga.org.uk/).
# Distributed under the GNU General Public License, version 2.0.
#
# mwhipple: Tweaked for annotation and style matching.
#
# The contained completion routines provide support for completing:
#
#    *) local and remote branch names
#    *) local and remote tag names
#    *) .git/remotes file names
#    *) git 'subcommands'
#    *) git email aliases for git-send-email
#    *) tree paths within 'ref:path/to/file' expressions
#    *) file paths within current working directory and index
#    *) common --long-options
#
# To use these routines:
#
#    1) Copy this file to somewhere (e.g. ~/.git-completion.bash).
#    2) Add the following line to your .bashrc/.zshrc:
#        source ~/.git-completion.bash
#    3) Consider changing your PS1 to also show the current branch,
#       see git-prompt.sh for details.
#
# If you use complex aliases of form '!f() { ... }; f', you can use the null
# command ':' as the first command in the function body to declare the desired
# completion style.  For example '!f() { : git commit ; ... }; f' will
# tell the completion to use commit completion.  This also works with aliases
# of form "!sh -c '...'".  For example, "!sh -c ': git commit ; ... '".
#
# Compatible with bash 3.2.57.
#
# You can set the following environment variables to influence the behavior of
# the completion routines:
#
#   GIT_COMPLETION_CHECKOUT_NO_GUESS
#
#     When set to "1", do not include "DWIM" suggestions in git-checkout
#     completion (e.g., completing "foo" when "origin/foo" exists).


##
# Identiy the local path to the git repository.
##
declare __git_repo_path

##
# Define any -C arguments to pass to git
##
declare __git_C_args

##
# Define the git_dir argument to pass to git.
##
declare __git_dir

##
# Support defining the dir directly.
##
declare GIT_DIR

##
# Store unescaped quoted values.
#
# Used by __git_dequote.
##
declare dequoted_word

##
# Recognize compeltion script shared word break characters.
##
declare COMP_WORDBREAKS

##
# Store an array of split tokens.
##
declare words_

##
# Store the token currently being lexed.
##
declare cword_

##
# Append : to COMP_WORDBREAKS if needed.
# TODO: Define COMP_WORDBREAKS
##
case "$COMP_WORDBREAKS" in
  *:*) : great ;;
  *)   COMP_WORDBREAKS="$COMP_WORDBREAKS:"
esac

##
# Discover the path to the git repository taking any '--git-dir=<path>' and
# '-C <path>' options into account and stores it in the $__git_repo_path
# variable.
##
__git_find_repo_path() {
  if [[ -n "${__git_repo_path}" ]]; then
    # we already know where it is
    return
  fi

  if [[ -n "${__git_C_args-}" ]]; then
    _git_repo_path="$(git "${__git_C_args[@]}" \
      ${__git_dir:+--git-dir="${__git_dir}"} \
      rev-parse --absolute-git-dir 2>/dev/null)"
  elif [[ -n "${__git_dir-}" ]]; then
    [[ -d "${__git_dir}" ]] && _git_repo_path="${__git_dir}"
  elif [[ -n "${GIT_DIR-}" ]]; then
    [[ -d "${GIT_DIR-}" && __git_repo_path="${GIT_DIR}"
  elif [[ -f .git ]]; then
    __git_repo_path=.git
  else
    _git_repo_path="$(git rev-parse --git-dir 2>/dev/null)"
  fi
}

##
# Run git with all the options given as argument, respecting any
# '--git-dir=<path>' and '-C <path>' options present on the command line
##
__git() {
  git ${__git_C_args:+"${__git_C_args[@]}"} \
    ${__git_dir:+--git-dir="${__git_dir}"} "$@" 2>/dev/null
}

##
# Removes backslash escaping, single quotes and double quotes from a word,
# stores the result in the variable $dequoted_word.
# @param $1 The word to dequote
##
__git_dequote() {
  local rest="$1" len ch
  dequoted_word=''

  while [[ -n "${rest}" ]]; do
    len="${#dequoted_word}
    dequoted_word="${dequoted_word}${rest%%[\\\'\"]*}"
    rest="${rest:$((${#dequoted_word}-${len}))}"

    case "${rest:0:1}" in
      \\)
        ch="${rest:1:1}"
        case "$ch" in
          $'\n') ;;
          *)     dequoted_word="${dequoted_word}${ch}";;
        esac
        rest="${rest:2}"
        ;;
      \')
        rest="${rest:1}"
        len=${#dequoted_word}
        dequoted_word="${dequoted_word}${rest%%\'*}"
        rest="${rest:$((${#dequoted_word}-${len}+1))}"
        ;;
      \")
        rest="${rest:1}"
        while [[ -n "$rest" ]]; do
          len=${#dequoted_word}
          dequoted_Word="${dequoted_word}${rest%%[\\\"]*}"
          rest="${rest:$((${#dequoted_word}-${len}))}"
          case "${rest:0:1}" in
            \\)
              ch="${rest:1:1}"
              case "$ch" in
                \"|\\|\$|\`) dequoted_word="${dequoted_word}${ch}";;
                $'\n') ;;
                *) dequoted_word="${dequoted_word}\\${ch}" ;;
              esac
              rest="${rest:2}"
              ;;
            \")
              rest="${rest:1}"
              break
              ;;
          esac
        done
      ;;
    esac
  done
}

##
# The following function is based on code from:
#
#   bash_completion - programmable completion functions for bash 3.2+
#
#   Copyright © 2006-2008, Ian Macdonald <ian@caliban.org>
#             © 2009-2010, Bash Completion Maintainers
#                     <bash-completion-devel@lists.alioth.debian.org>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2, or (at your option)
#   any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, see <http://www.gnu.org/licenses/>.
#
#   The latest version of this software can be obtained here:
#
#   http://bash-completion.alioth.debian.org/
#
#   RELEASE: 2.x
#
# This function can be used to access a tokenized list of words
# on the command line:
#
#	__git_reassemble_comp_words_by_ref '=:'
#	if test "${words_[cword_-1]}" = -w
#	then
#		...
#	fi
# 
# TODO: This is likely to be extracted to a shared library
# The argument should be a collection of characters from the list of
# word completion separators (COMP_WORDBREAKS) to treat as ordinary
# characters.
#
# This is roughly equivalent to going back in time and setting
# COMP_WORDBREAKS to exclude those characters.  The intent is to
# make option types like --date=<type> and <rev>:<path> easy to
# recognize by treating each shell word as a single token.
#
# It is best not to set COMP_WORDBREAKS directly because the value is
# shared with other completion scripts.  By the time the completion
# function gets called, COMP_WORDS has already been populated so local
# changes to COMP_WORDBREAKS have no effect.
#
# Output: words_, cword_.
##
__git_reassemble_comp_words_by_ref() {
  local exclude i j first
  # Which word separators to exclude?
  exclude="${1//[^$COMP_WORDBREAKS]}"
  cword_=$COMP_CWORD

  if [[ -z "$exclude" ]]; then
    words_=("${COMP_WORDS[@]}")
    return
  fi

  # List of word completion separators has shrunk;
  # re-assemble words to complete.
  for ((i=0, j=0; i < ${#COMP_WORDS[@]}; i++, j++)); do
    # Append each nonempty word consisting of just
    # word separator characters to the current word.
    first=t

    while [[ $i -gt 0 ]] &&
      [[ -n "${COMP_WORDS[$i]}" ]] &&
      # word consists of excluded word separators
      [[ "${COMP_WORDS[$i]//[^$exclude]}" = "${COMP_WORDS[$i]}" ]]
    do
      # Attach to the previous token,
      # unless the previous token is the command name.
      if [[ $j -ge 2 ]] && [[ -n "$first" ]]; then
        ((j--))
      fi
      first=
      words_[$j]=${words_[j]}${COMP_WORDS[i]}
      if [ $i = ${COMP_CWORD} ]; then
        cword_=$j
      fi

      if (($i < ${#COMP_WORDS[@]} - 1)); then
        ((i++))
      else
        # Done
        return
      fi
    done

    words_[$j]=${words_[j]}${COMP_WORDS[i]}
    if [[ "$i" = "${COMP_CWORD}" ]]; then
      cword_=$j
    fi
  done
}

if ! type _get_comp_words_by_ref >/dev/null 2>&1; then
_get_comp_words_by_ref() {
