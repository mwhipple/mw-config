##
# @file go.sh
# Configure settings for Go development.
##

GOPATH="$HOME/gopath"
PATH="$PATH:$GOPATH/bin"
