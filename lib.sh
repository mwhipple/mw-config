##
# @file lib.sh
# Provide some function definitions to use elsewhere.
##

set -euo pipefail

##
# Append a line to a file if it is not already in the file.
#
# If the file is missing, it will be created with the line.
#
# @param $1 Define file in which line will be present.
# @param $2 Provide line that will be added if not present.
##
append_if_missing() {
    if [[ ! -f "$1" || -z "$(grep -Fx "$2" "$1")" ]]; then
	echo "$2" >> "$1"
    else
	echo "Already present."
    fi
}
append_if_missing_spec() {
    TMPDIR=$(mktemp -d)
    file_prefix="${TMPDIR}mw-config_append_if_missing"
    file="${file_prefix}"
    rm -f "${file}"

    append_if_missing "${file}" 'File is created'
    check_equal 'File is created' "$(<"${file}")"

    append_if_missing "${file}" 'Line is added only once'
    check_equal $'File is created\nLine is added only once' "$(<"${file}")"

    append_if_missing "${file}" 'Line is added only once'
    check_equal $'File is created\nLine is added only once' "$(<"${file}")"

    check_match '*No such file or directory*' \
		"$(append_if_missing "/tmp/$PPID/invalid_directory" 'Should throw an error' 2>&1)"
}
