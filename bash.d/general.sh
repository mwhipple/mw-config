##
# @file general.sh
# Perform some general bite-sized bash configutation.
#
# This includes base behavior and anything too simple
# to warrant splitting out.
##

# Configure default locale.
LC_ALL='en_US.UTF-8'
LANG='en_US.UTF-8'

# Disable terminal START/STOP.
# That can interfere with some emacs-y commands.
stty -ixon

# Add a home bin directory to the path.
export PATH="${PATH}:~/bin"

# Include locally installed info files.
export INFOPATH="${INFOPATH}:/usr/local/share/info"

# Silence warning on OS X but just set everywhere for simplicity.
export BASH_SILENCE_DEPRECATION_WARNING=1

# Customize the primary prompt.
PS1='\u@\h:\w\$ '
