SSH_ENV="${HOME}/.ssh/environment"

##
# Start an SSH agent and store the information in a sourceable file.
##
start_agent() {
    echo 'Initializing new SSH agent...'
    ssh-agent | sed 's/^echo/#echo/' > "${SSH_ENV}"
    chmod 600 "${SSH_ENV}"
    . "${SSH_ENV}" > /dev/null
    /usr/bin/ssh-add
}

##
# Source variables for existing ssh agent if they exist.
##
if [[ -f "${SSH_ENV}" ]]; then
    . "${SSH_ENV}" > /dev/null
fi

##
# If environment variables don't exist or pid seems invalid, start agent.
##
[[ -n ${SSH_AGENT_PID} ]] || [[ -d /proc/${SSH_AGENT_PID:-'MISSING'} ]] || start_agent
