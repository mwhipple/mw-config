##
# @file aws.sh
# Define some helpers for working with AWS.
##

##
# Define the AWS_CONFIG_FILE to use, defaults to '$HOME/.aws/config'.
##
declare AWS_CONFIG_FILE="${AWS_CONFIG_FILE:-${HOME}/.aws/config}"

##
# Define the AWS profile to use by default.
##
declare AWS_PROFILE='default'

##
# Define the ARN of the role to assume for the selected profile.
##
declare AWS_ROLE_ARN

##
# Set AWS_MFA_SERIAL to the MFA configured for the current profile.
##
aws::lookup_mfa_serial() {
    AWS_MFA_SERIAL=$(awk -s "BEGIN { thisP=0; } /\[.*]/ {thisP=(\$0==\"[${AWS_PROFILE}]\")} /mfa_serial/ { if (thisP) { print \$3 } }" "${AWS_CONFIG_FILE}") 
}

##
# Parse AWS credentials into standard environment variables.
#
# This augments STS commands which acquire credentials
# so that responses are in an expected format,
# and then parses that response into the typical environment
# variables:
#  AWS_ACCESS_KEY_ID
#  AWS_SECRET_ACCESS_KEY
#  AWS_SESION_TOKEN
#
# Those variables are also exported.
##
aws::load_credentials_from() {
    # Clear any existing token which could mess with request.
    unset AWS_SESSION_TOKEN
    local aws_exec_result=$($@ --query 'Credentials.[AccessKeyId,SecretAccessKey,SessionToken]' --output text) ||
	echo "Failure acquiring STS ticket!"
    read AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY AWS_SESSION_TOKEN <<< ${aws_exec_result}
    export AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY AWS_SESSION_TOKEN
}

##
# Load a session token using the MFA for the configured profile.
#
# If AWS_MFA_SERIAL is explicitly set, that MFA will be used.
# This calls aws::load_credentials_from and so produces the same side effects.
##
aws_load_mfa_session_token() {
    [[ -n "${AWS_MFA_SERIAL}" ]] || aws::lookup_mfa_serial
    read -s -p "Enter MFA code for ${AWS_MFA_SERIAL}:" token
    aws::load_credentials_from aws sts get-session-token --serial-number "${AWS_MFA_SERIAL}" --token-code "${token}" --profile="${AWS_PROFILE}"
    echo
}

##
# Call AWS STS to acquire a ticket and load and export resulting values into environment.
#
# @param $1 Provide the name of the configured profile for this session.
# @param $2 Provide the ARN of the role to assume.
# @output Set AWS_ACCESS_KEY_ID to the retrieved IAM key.
# @output Set AWS_SECRET_ACCESS_KEY to the retrieved IAM secret.
# @output Set AWS_SESSION_TOKEN to the token for the current STS session.
##
aws::assume_profile() {
  aws::load_credentials_from aws sts assume-role --profile "${1}" --role-arn "${2}" --role-session-name "${1}-session"
  echo
}

##
# Prompt to select an AWS Profile from a conventionally formatted AWS
# config file.
#
# The user will be presented with a menu from which to choose a
# profile.
#
# @output Set AWS_PROFILE to the selected profile.
# @output Set AWS_ROLE_ARN to the ARN of the role for AWS_PROFILE.
##
aws::select_profile() {
  PS3='Select an AWS Profile #: '
  select AWS_PROFILE in $(awk -s '/\[profile/ {print substr($2, 0, length($2)-1)}' "${AWS_CONFIG_FILE}"); do
    echo ${AWS_CONFIG_FILE}
    AWS_ROLE_ARN=$(awk -s "BEGIN { thisP=0; } /\[profile/ {thisP=(\"$AWS_PROFILE\"==substr(\$2, 0, length(\$2)-1)) } /role_arn/ { if (thisP) print \$3 }" "${AWS_CONFIG_FILE}")
    break
  done
}

##
# Prompt for which AWS profile to use and call STS to assume the associated role.
#
# This is a composition of #select_aws_profile and #aws_assume_profile.
#
# @output Set AWS_PROFILE to the selected profile.
# @output Set AWS_ROLE_ARN to the ARN of the role associated with AWS_PROFILE.
# @output Set AWS_ACCESS_KEY_ID to the retrieved IAM key.
# @output Set AWS_SECRET_ACCESS_KEY to the retrieved IAM secret.
# @output Set AWS_SESSION_TOKEN to the token for the current STS session.
##
aws_assume() {
  select_aws_profile
  aws_assume_profile ${AWS_PROFILE} ${AWS_ROLE_ARN}
}
