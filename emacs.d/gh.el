(defun gh-fetch (url)
  (let ((url-request-extra-headers
	 '(("Accept" . "application/vnd.github.v3+json"))))
    (url-retrieve
     url
     (lambda (&rest ignored)
       (url-http-parse-response)
       (goto-char (point-min))
       (search-forward "\n\n")
       (message (string (json-read)))))))

(gh-fetch "https://api.github.com")
