;; init.el - Configure my emacs.
(progn					; USE-PACKAGE
  (or (package-installed-p 'use-package)
      (warn "use-package is not installed, expect bad things!"))
  (require 'use-package)
  (require 'bind-key))
(bind-keys :map global-map		; CORE
	   ("C-g" . keyboard-quit)
	   ("M-x" . execute-extended-command))
(progn					; PACKAGING
  (require 'package)
  (setq package-archives '(("gnu" . "https://elpa.gnu.org/packages/")
			   ("melpa" . "https://melpa.org/packages/"))))
(progn					; DISPLAY
  (menu-bar-mode -1)
  (tool-bar-mode -1)
  (add-to-list 'default-frame-alist '(background-color . "black"))
  (add-to-list 'default-frame-alist '(foreground-color . "white")))

(progn					; MODE-LINE
  (setq display-time-day-and-date t)
  (display-time-mode 1)
  (display-battery-mode 1)
  
  ;; Customize the mode line.
  (setq-default
   mode-line-format
   (list
    "%b L%l %p "
    mode-line-misc-info)))

(bind-keys :map global-map		; INPUT
	   ;; Lower
	   ("a" . self-insert-command)
	   ("b" . self-insert-command)
	   ("c" . self-insert-command)
	   ("d" . self-insert-command)
	   ("e" . self-insert-command)
	   ("f" . self-insert-command)
	   ("g" . self-insert-command)
	   ("h" . self-insert-command)
	   ("i" . self-insert-command)
	   ("j" . self-insert-command)
	   ("k" . self-insert-command)
	   ("l" . self-insert-command)
	   ("m" . self-insert-command)
	   ("n" . self-insert-command)
	   ("o" . self-insert-command)
	   ("p" . self-insert-command)
	   ("q" . self-insert-command)
	   ("r" . self-insert-command)
	   ("s" . self-insert-command)
	   ("t" . self-insert-command)
	   ("u" . self-insert-command)
	   ("v" . self-insert-command)
	   ("w" . self-insert-command)
	   ("x" . self-insert-command)
	   ("y" . self-insert-command)
	   ("z" . self-insert-command)

	   ;; Upper
	   ("A" . self-insert-command)
	   ("B" . self-insert-command)
	   ("C" . self-insert-command)
	   ("D" . self-insert-command)
	   ("E" . self-insert-command)
	   ("F" . self-insert-command)
	   ("G" . self-insert-command)
	   ("H" . self-insert-command)
	   ("I" . self-insert-command)
	   ("J" . self-insert-command)
	   ("K" . self-insert-command)
	   ("L" . self-insert-command)
	   ("M" . self-insert-command)
	   ("N" . self-insert-command)
	   ("O" . self-insert-command)
	   ("P" . self-insert-command)
	   ("Q" . self-insert-command)
	   ("R" . self-insert-command)
	   ("S" . self-insert-command)
	   ("T" . self-insert-command)
	   ("U" . self-insert-command)
	   ("V" . self-insert-command)
	   ("W" . self-insert-command)
	   ("X" . self-insert-command)
	   ("Y" . self-insert-command)
	   ("Z" . self-insert-command)

	   ;; Numbers
	   ("0" . self-insert-command)
	   ("1" . self-insert-command)
	   ("2" . self-insert-command)
	   ("3" . self-insert-command)
	   ("4" . self-insert-command)
	   ("5" . self-insert-command)
	   ("6" . self-insert-command)
	   ("7" . self-insert-command)
	   ("8" . self-insert-command)
	   ("9" . self-insert-command)

	   ;; Symbols
	   ("`" . self-insert-command)
	   ("~" . self-insert-command)
	   ("!" . self-insert-command)
	   ("@" . self-insert-command)
	   ("#" . self-insert-command)
	   ("$" . self-insert-command)
	   ("%" . self-insert-command)
	   ("^" . self-insert-command)
	   ("&" . self-insert-command)
	   ("*" . self-insert-command)
	   ("(" . self-insert-command)
	   (")" . self-insert-command)
	   ("-" . self-insert-command)
	   ("_" . self-insert-command)
	   ("=" . self-insert-command)
	   ("+" . self-insert-command)
	   ("[" . self-insert-command)
	   ("]" . self-insert-command)
	   ("{" . self-insert-command)
	   ("}" . self-insert-command)
	   ("\\" . self-insert-command)
	   ("|" . self-insert-command)
	   (";" . self-insert-command)
	   (":" . self-insert-command)
	   ("'" . self-insert-command)
	   ("\"" . self-insert-command)
	   ("," . self-insert-command)
	   ("." . self-insert-command)
	   ("<" . self-insert-command)
	   (">" . self-insert-command)
	   ("/" . self-insert-command)
	   ("?" . self-insert-command)

	   ;; Utility
	   ("C-q" . quoted-insert))

(bind-keys :map global-map		; MOTION
	   ("C-f"   . forward-char)
	   ("C-n"   . next-line)
	   ("C-p"   . previous-line)
	   ("M-f"   . forward-word)
	   ("M-b"   . backward-word)
	   ("C-a"   . move-beginning-of-line)
	   ("C-M-n" . forward-list)
	   ("C-M-p" . backward-list))
(progn
  (define-key global-map (kbd "C-e") 'move-end-of-line)
  (define-key global-map (kbd "M-<") 'beginning-of-buffer)
  (define-key global-map (kbd "M->") 'end-of-buffer)
  (define-key global-map (kbd "C-v") 'scroll-up-command)
  (define-key global-map (kbd "M-v") 'scroll-down-command)
  (define-key global-map (kbd "C-s") 'isearch-forward)
  (define-key global-map (kbd "C-r") 'isearch-backward))

(bind-keys :map global-map	; EDITING
	   ("C-d" . delete-char)
	   ("C-o" . open-line)
	   ("C-w" . kill-region)
	   ("M-w" . kill-ring-save)
	   ("C-j" . electric-newline-and-maybe-indent)
	   ("M-u" . upcase-word))
(progn				
  (define-key global-map (kbd "C-k") 'kill-line)
  (define-key global-map (kbd "M-d") 'kill-word)
  (define-key global-map (kbd "DEL") 'delete-backward-char)
  (define-key global-map (kbd "RET") 'newline)
  (bind-keys :map global-map
	     ("M-\\" . delete-horizontal-space)
	     ("M-%"  . query-replace))
  ;; Chrome OS support
  (define-key global-map (kbd "<deletechar>") 'backward-kill-word))
(bind-keys :map global-map		; MARK/RING
	   ("C-SPC" . set-mark-command)
	   ("C-y"   . yank)
	   ("M-y"   . yank-pop))
(progn					; NAVIGATION
  (define-key global-map (kbd "M-.") 'xref-find-definitions))
(progn					; FILE MANAGEMENT
  (define-key global-map (kbd "C-x C-f") 'ffap)
  (define-key global-map (kbd "C-x C-s") 'save-buffer)
  (use-package vc
    :bind (("C-x v v" . vc-next-action)
	   ("C-x v d" . vc-dir)
	   :map vc-dir-mode-map
	   ("M-m" . vc-dir-mark)
	   ("M-u" . vc-dir-unmark)
	   ("M-=" . 'vc-diff))))

(bind-keys :prefix "C-h"
	   :prefix-map help-map
	   :prefix-docstring "Help me!"
	   ("b" . describe-bindings)
	   ("f" . describe-function)
	   ("i" . info)
	   ("k" . describe-key)
	   ("p" . describe-package)
	   ("v" . describe-variable)
	   ("w" . where-is))
(progn					; HELP
  (define-key global-map (kbd "C-h l") 'view-lossage)
  (define-key global-map (kbd "C-h m") 'describe-mode))
(use-package exec-path-from-shell
  :ensure t
  :custom
  (exec-path-from-shell-variables
   '("SSH_AUTH_SOCK"
     "SSH_AGENT_PID"))
  :config
  (exec-path-from-shell-initialize))
(progn					; IPC
  (define-key global-map (kbd "M-!") 'shell-command))

(bind-keys :prefix "C-x C-m"
	   :prefix-map x-mode-map
	   :prefix-docstring
	   "Enter some modes from anywhere."
	   ("s" . shell)
	   ("v" . view-mode))

(use-package org-mode			; ORG
  :mode "\\.org\\'"
  :bind (:prefix "C-x C-o"
	 :prefix-map x-org
	 :prefix-docstring "Globally bind org keys."
	 ("a" . org-agenda)
	 ("c" . org-capture)
	 ("l" . org-store-link)
	 :map org-mode-map
	 ("C-c C-w"  . org-refile)
	 ("C-c C-s"  . org-schedule)
	 ("C-c C-d"  . org-deadline)
	 ("M-<down>" . org-metadown)
	 ;; For chromebook
	 ("<next>"   . org-metadown)
	 ("M-<up>"   . org-metaup)
	 ("<prior>"   . org-metaup)
	 ("C-M-_"    . org-cycle)
	 ("C-c C-t"  . org-todo)
	 ("C-c C-r"  . org-reveal)
	 ("C-c C-k"  . outline-show-branches))
  :custom
  (org-cycle-global-at-bob t)
  (org-agenda-span 'day)
  (org-agenda-sorting-strategy
   '((agenda time-up priority-down timestamp-up)
     (todo deadline-down scheduled-down)
     (tags priority-down category-keep)
     (search category-keep)))
  (org-agenda-files '("~/Documents/hq"
		      "~/Documents/hq/fin")))
(progn					; GLOBAL STYLE
  (setq-default require-final-newline t))
(progn					; BOOKMARKS
  ;; TODO: Move to C-x C-r m
  (define-key global-map (kbd "C-x r m") 'bookmark-set)
  (setq bookmark-save-flag 1)
  (desktop-save-mode))
(progn					; DEV
  (add-hook
   'prog-mode-hook
   (lambda ()
     (hs-minor-mode)
     (define-key prog-mode-map (kbd "C-M-_") 'hs-toggle-hiding)))

  (add-to-list 'auto-mode-alist '("\\.el\\'" . emacs-lisp-mode))
  (define-key global-map (kbd "C-x C-e") 'eval-last-sexp)

  (define-key global-map (kbd "M-SPC") 'complete-symbol)

  (add-to-list 'auto-mode-alist '("\\.c\\'" . c-mode))
  (define-key global-map (kbd "M-;") 'comment-dwim)
  (define-key global-map (kbd "C-M-n") 'forward-list)
  (define-key global-map (kbd "C-M-p") 'backward-list))
(progn					; MAIL
  (define-key global-map (kbd "C-x C-m g") 'gnus)
  (define-key global-map (kbd "C-x C-m m") 'gnus-summary-mail-other-window)
  (setq gnus-select-method
	'(nnimap "imap.gmail.com"
		 (nnimap-server-port "imaps")))

  (add-hook
   'gnus-mode-hook
   (lambda ()
     (define-key gnus-group-mode-map (kbd "M-g") 'gnus-group-get-new-news-this-group)
     (define-key gnus-group-mode-map (kbd "M-<down>") (lambda () (interactive) (gnus-group-read-group 50)))

     (define-key gnus-summary-mode-map (kbd "M-<down>") 'gnus-summary-next-page)
     (define-key gnus-summary-mode-map (kbd "C-c m") 'gnus-summary-move-article))))
(progn					; WEB
  (define-key global-map (kbd "C-x C-m w") 'eww)
  (add-hook
   'eww-mode-hook
   (lambda ()
     (define-key eww-mode-map (kbd "C-c w") 'eww-copy-page-url)))
  (setq browse-url-browser-function 'eww-browse-url))
(progn					; MUSIC
  (defvar LilyPond-mode-map ()
    "Keymap used in `LilyPond-mode' buffers.")

  (defun LilyPond-mode ()
    "Major mode for editing LilyPond music files.

This mode knows about LilyPond keywords and line comments, not about
indentation or block comments.  It features easy compilation, error
finding and viewing of a LilyPond source buffer or region.

COMMANDS
\\{LilyPond-mode-map}
VARIABLES

LilyPond-command-alist\t\talist from name to command"
    (interactive)

    ;; set up local variables
    (kill-all-local-variables))

  (add-to-list 'auto-mode-alist '("\\.ly$" . LilyPond-mode))
  (add-to-list 'auto-mode-alist '("\\.ily$" . LilyPond-mode)))
