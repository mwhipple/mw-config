;; init.el - Configure my emacs.

(progn					; CORE
  (define-key global-map (kbd "C-g") 'keyboard-quit)
  (define-key global-map (kbd "M-x") 'execute-extended-command))
(progn 					; PACKAGING
  (require 'package)
  (setq package-archives '(("gnu" . "https://elpa.gnu.org/packages/")
			   ("melpa" . "https://melpa.org/packages/"))))
(progn					; DISPLAY
  (menu-bar-mode -1)
  (tool-bar-mode -1)
  (setq display-time-day-and-date t)
  (display-time-mode 1)
  (display-battery-mode 1)

  (add-to-list 'default-frame-alist '(background-color . "black"))
  (add-to-list 'default-frame-alist '(foreground-color . "white"))
  
  ;; Customize the mode line.
  (setq-default
   mode-line-format
   (list
    "%b L%l %p "
    mode-line-misc-info)))
(progn					; INPUTS
  (define-key global-map "a" 'self-insert-command)
  (define-key global-map "b" 'self-insert-command)
  (define-key global-map "c" 'self-insert-command)
  (define-key global-map "d" 'self-insert-command)
  (define-key global-map "e" 'self-insert-command)
  (define-key global-map "f" 'self-insert-command)
  (define-key global-map "g" 'self-insert-command)
  (define-key global-map "h" 'self-insert-command)
  (define-key global-map "i" 'self-insert-command)
  (define-key global-map "j" 'self-insert-command)
  (define-key global-map "k" 'self-insert-command)
  (define-key global-map "l" 'self-insert-command)
  (define-key global-map "m" 'self-insert-command)
  (define-key global-map "n" 'self-insert-command)
  (define-key global-map "o" 'self-insert-command)
  (define-key global-map "p" 'self-insert-command)
  (define-key global-map "q" 'self-insert-command)
  (define-key global-map "r" 'self-insert-command)
  (define-key global-map "s" 'self-insert-command)
  (define-key global-map "t" 'self-insert-command)
  (define-key global-map "u" 'self-insert-command)
  (define-key global-map "v" 'self-insert-command)
  (define-key global-map "w" 'self-insert-command)
  (define-key global-map "x" 'self-insert-command)
  (define-key global-map "y" 'self-insert-command)
  (define-key global-map "z" 'self-insert-command)

  (define-key global-map "A" 'self-insert-command)
  (define-key global-map "B" 'self-insert-command)
  (define-key global-map "C" 'self-insert-command)
  (define-key global-map "D" 'self-insert-command)
  (define-key global-map "E" 'self-insert-command)
  (define-key global-map "F" 'self-insert-command)
  (define-key global-map "G" 'self-insert-command)
  (define-key global-map "H" 'self-insert-command)
  (define-key global-map "I" 'self-insert-command)
  (define-key global-map "J" 'self-insert-command)
  (define-key global-map "K" 'self-insert-command)
  (define-key global-map "L" 'self-insert-command)
  (define-key global-map "M" 'self-insert-command)
  (define-key global-map "N" 'self-insert-command)
  (define-key global-map "O" 'self-insert-command)
  (define-key global-map "P" 'self-insert-command)
  (define-key global-map "Q" 'self-insert-command)
  (define-key global-map "R" 'self-insert-command)
  (define-key global-map "S" 'self-insert-command)
  (define-key global-map "T" 'self-insert-command)
  (define-key global-map "U" 'self-insert-command)
  (define-key global-map "V" 'self-insert-command)
  (define-key global-map "W" 'self-insert-command)
  (define-key global-map "X" 'self-insert-command)
  (define-key global-map "Y" 'self-insert-command)
  (define-key global-map "Z" 'self-insert-command)

  (define-key global-map "1" 'self-insert-command)
  (define-key global-map "2" 'self-insert-command)
  (define-key global-map "3" 'self-insert-command)
  (define-key global-map "4" 'self-insert-command)
  (define-key global-map "5" 'self-insert-command)
  (define-key global-map "-" 'self-insert-command)

  (define-key global-map (kbd "RET") 'newline))
(progn					; MOTION
  (define-key global-map (kbd "C-n") 'next-line)
  (define-key global-map (kbd "C-p") 'previous-line)
  (define-key global-map (kbd "C-a") 'move-beginning-of-line)
  (define-key global-map (kbd "C-e") 'move-end-of-line)
  (define-key global-map (kbd "M-<") 'beginning-of-buffer)
  (define-key global-map (kbd "M->") 'end-of-buffer)
  (define-key global-map (kbd "C-v") 'scroll-up-command)
  (define-key global-map (kbd "M-v") 'scroll-down-command)
  (define-key global-map (kbd "C-s") 'isearch-forward)
  (define-key global-map (kbd "C-r") 'isearch-backward))
(progn					; EDITING
  (define-key global-map (kbd "C-k") 'kill-line)
  (define-key global-map (kbd "DEL") 'delete-backward-char)
  ;; Chrome OS support
  (define-key global-map (kbd "<deletechar>") 'backward-kill-word))
(progn					; NAVIGATION
  (define-key global-map (kbd "M-.") 'xref-find-definitions))
(progn					; FILE MANAGEMENT
  (define-key global-map (kbd "C-x C-f") 'ffap)
  (define-key global-map (kbd "C-x C-s") 'save-buffer)
  (define-key global-map (kbd "C-x v v") 'vc-next-action)
  (define-key global-map (kbd "C-x v d") 'vc-dir)
  (add-hook
   'vc-dir-mode-hook
   (lambda ()
     (define-key vc-dir-mode-map (kbd "M-m") 'vc-dir-mark)
     (define-key vc-dir-mode-map (kbd "M-u") 'vc-dir-unmark)
     (define-key vc-dir-mode-map (kbd "M-=") 'vc-diff))))
(progn					; HELP
  (define-key global-map (kbd "C-h b") 'describe-bindings)
  (define-key global-map (kbd "C-h f") 'describe-function)
  (define-key global-map (kbd "C-h k") 'describe-key)
  (define-key global-map (kbd "C-h l") 'view-lossage)
  (define-key global-map (kbd "C-h m") 'describe-mode))
(progn					; IPC
  ;; Stolen from exec-path-from-shell
  ;; exec-path-from-shell-printf
  (defun ishell-printf (str &optional args)
     "Return the result of printing STR in the user's shell.

Executes the shell as interactive login shell.

STR is inserted literally in a single-quoted argument to printf,
and may therefore contain backslashed escape sequences understood
by printf.

ARGS is an optional list of args which will be inserted by printf
in place of any % placeholders in STR.  ARGS are not automatically
shell-escaped, so they may contain $ etc.")
     
  ;; exec-path-from-shell-getenvs
  (defun envvars-from-ishell (names)
    "Get the environment variables with NAMES from the user's shell.

Execute the shell according to `exec-path-from-shell-arguments'.
The result is a list of (NAME . VALUE) pairs."
    (if (file-remote-p default-directory)
      (error
       "Remote directories are not supported for envvars-from-shell"))
    (let* ((random-default
	    (md5 (format "%s%s%s"
			 (emacs-pid) (random) (current-time))))
	   (dollar-it (lambda (n) (format "${%s-%s}" n random-default)))
	   (dollar-names (mapcar dollar-it names)))))
	   
  ; (exec-path-from-shell-copy-envs exec-path-from-shell-variables)  

  (define-key global-map (kbd "M-!") 'shell-command))
(progn					; SHELL
  (define-key global-map (kbd "C-x C-m s") 'shell))
(progn					; ORG
  (add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode))
  (add-hook
   'org-mode-hook
   (lambda()
     (define-key org-mode-map (kbd "C-c C-w") 'org-refile)
     (define-key org-mode-map (kbd "M-<down>") 'org-metadown)
     ;; For chromebook
     (define-key org-mode-map (kbd "<next>") 'org-metadown)
     (define-key org-mode-map (kbd "M-<up>") 'org-metaup)
     (define-key org-mode-map (kbd "C-M-_") 'org-cycle)
     (define-key org-mode-map (kbd "C-c C-t") 'org-todo)))
  
  (setq
   org-agenda-files
   '(
     "~/Documents/hq"
     "~/Documents/hq/fin"   
     ))

  ;; TODO: Move to C-x C-o a
  (define-key global-map (kbd "C-x C-a") 'org-agenda)
  ;; TODO: Move to C-x C-r m
  (define-key global-map (kbd "C-x r m") 'bookmark-set)
  (setq bookmark-save-flag 1))
(progn					; DEV
  (add-hook
   'prog-mode-hook
   (lambda ()
     (hs-minor-mode)
     (define-key prog-mode-map (kbd "C-M-_") 'hs-toggle-hiding)))

  (add-to-list 'auto-mode-alist '("\\.el\\'" . emacs-lisp-mode))
  (define-key global-map (kbd "C-x C-e") 'eval-last-sexp)

  (define-key global-map (kbd "M-SPC") 'complete-symbol)

  (add-to-list 'auto-mode-alist '("\\.c\\'" . c-mode))
  (define-key global-map (kbd "M-;") 'comment-dwim)
  (define-key global-map (kbd "C-M-n") 'forward-list)
  (define-key global-map (kbd "C-M-p") 'backward-list))
(progn					; MAIL
  (define-key global-map (kbd "C-x C-m g") 'gnus)
  (define-key global-map (kbd "C-x C-m m") 'gnus-summary-mail-other-window)
  (setq gnus-select-method
	'(nnimap "imap.gmail.com"
		 (nnimap-server-port "imaps")))

  (add-hook
   'gnus-mode-hook
   (lambda ()
     (define-key gnus-group-mode-map (kbd "M-g") 'gnus-group-get-new-news-this-group)
     (define-key gnus-group-mode-map (kbd "M-<down>") (lambda () (interactive) (gnus-group-read-group 50)))

     (define-key gnus-summary-mode-map (kbd "M-<down>") 'gnus-summary-next-page)
     (define-key gnus-summary-mode-map (kbd "C-c m") 'gnus-summary-move-article))))
(progn					; WEB
  (define-key global-map (kbd "C-x C-m w") 'eww)
  (define-key eww-mode-map (kbd "C-c w") 'eww-copy-page-url)
  (setq browse-url-browser-function 'eww-browse-url))
(progn					; MUSIC
  (defvar LilyPond-mode-map ()
    "Keymap used in `LilyPond-mode' buffers.")

  (defun LilyPond-mode ()
    "Major mode for editing LilyPond music files.

This mode knows about LilyPond keywords and line comments, not about
indentation or block comments.  It features easy compilation, error
finding and viewing of a LilyPond source buffer or region.

COMMANDS
\\{LilyPond-mode-map}
VARIABLES

LilyPond-command-alist\t\talist from name to command"
    (interactive)

    ;; set up local variables
    (kill-all-local-variables))

  (add-to-list 'auto-mode-alist '("\\.ly$" . LilyPond-mode))
  (add-to-list 'auto-mode-alist '("\\.ily$" . LilyPond-mode)))
